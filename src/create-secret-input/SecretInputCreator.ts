import BN from "bn.js";
import {RealEngineState} from "./real/RealTypes";
import {RealPayload} from "./real/RealPayload";
import {RealShareWriter} from "./real/RealBinaryShareWriter";
import {BufferWriter} from "./util/BufferWriter";

export function buildSecretInputRpc(engines: RealEngineState, actionShortName: number, secretInput: number): Buffer {
    const payloadBuilder = new RealPayload(engines);

    const secretValueWriter = (writer: RealShareWriter) => {
        const bn = new BN(secretInput, "le");
        writer.writeLong(bn, 32);
    };
    const writeAdditional = (writer: BufferWriter): void => {
        writer.writeByte(actionShortName);
    }

    return payloadBuilder.onChainZkInput(
        secretValueWriter,
        writeAdditional
    )
}