import BN from "bn.js";
import { BinaryShareWriter } from "../util/BinaryShareWriter";
import { BinaryExtensionFieldElementFactory as factory } from "./BinaryExtensionFieldElement";
import { initializeShareBuffers, writeShares } from "./RealShare";

export interface RealShareWriter {
  writeLong(value: BN, bitLength: number): RealShareWriter;

  writeInt(value: number, bitLength: number): RealShareWriter;

  writeBoolean(bool: boolean): RealShareWriter;

  bitLengths(): number[];

  toShares(randomGenerator: (ln: number) => Buffer): Buffer[];
}

/**
 * A share writer for binary REAL.
 */
export class RealBinaryShareWriter {
  private readonly writer: BinaryShareWriter;
  private readonly providedBitLengths: number[];

  private constructor(writer: BinaryShareWriter) {
    this.writer = writer;
    this.providedBitLengths = [];
  }

  /**
   * Create a share writer for binary REAL.
   *
   * @returns the writer
   */
  public static create(): RealBinaryShareWriter {
    return new RealBinaryShareWriter(BinaryShareWriter.create());
  }

  /**
   * Return the bit lengths of the invocations so far.
   *
   * @returns a list containing the bit lengths of each invocation to this reader.
   */
  public bitLengths(): number[] {
    return this.providedBitLengths;
  }

  /**
   * Secret-share all invocations so far.
   *
   * @param random a pseudorandom generator
   * @returns a buffer containing the shares
   */
  public toShares(random: (ln: number) => Buffer): Buffer[] {
    const compactValues = this.writer.toBuffer();
    const n = this.writer.writtenBits();
    const bits = Buffer.alloc(n);
    let i = 0;
    for (; i < compactValues.length - 1; i++) {
      const byte = compactValues[i];
      for (let j = 0; j < 8; j++) {
        bits[i * 8 + j] = (byte >> j) & 1;
      }
    }
    for (let j = 0; j < n % 8; j++) {
      bits[i * 8 + j] = (compactValues[i] >> j) & 1;
    }
    return RealBinaryShareWriter.createShares(bits, random);
  }

  private static createShares(values: Buffer, random: (ln: number) => Buffer): Buffer[] {
    const bitLength = values.length;
    const computationAlphas = factory.computationAlphas();

    const shares: Buffer[] = initializeShareBuffers(computationAlphas.length, bitLength);
    for (let i = 0; i < bitLength; i++) {
      const randomCoefficient = random(1)[0] & 0xf;
      const coefficients = [values[i], randomCoefficient].map(factory.createElement);

      const polynomial = factory.createPoly(coefficients);
      for (const alpha of computationAlphas) {
        const share = polynomial.evaluate(alpha);
        const shareBytes = Buffer.alloc(1);
        shareBytes[0] = share.value;
        writeShares(shareBytes, shares[alpha.value - 1], i);
      }
    }
    return shares;
  }

  /**
   * Prepare a boolean to be shared.
   *
   * @param bool the boolean to be shared.
   * @returns this
   */
  public writeBoolean(bool: boolean): RealShareWriter {
    this.writer.writeBoolean(bool);
    this.providedBitLengths.push(1);
    return this;
  }

  /**
   * Prepare an integer (a numeric value of 32 bits or less) to be shared.
   *
   * @param value the value to be shared
   * @param bitLength the bit length of the value. Cannot exceed 32.
   * @returns this
   */
  public writeInt(value: number, bitLength: number): RealShareWriter {
    this.writer.writeInt(value, bitLength);
    this.providedBitLengths.push(bitLength);
    return this;
  }

  /**
   * Prepare an arbitrary size integer to be shared.
   *
   * @param value the value to be shared
   * @param bitLength the bitlength of value. Must be able to hold value
   * @returns this
   *
   */
  public writeLong(value: BN, bitLength: number): RealShareWriter {
    this.writer.writeLong(value, bitLength);
    this.providedBitLengths.push(bitLength);
    return this;
  }
}
