import { randomBytes } from "crypto";
import { ec as Elliptic } from "elliptic";
import { BufferWriter } from "../util/BufferWriter";
import { createAesForParty, generateKeyPair } from "../util/CryptoUtils";
import { RealBinaryShareWriter } from "./RealBinaryShareWriter";
import { RealEngineState } from "./RealTypes";

export class RealPayload {
    private static readonly ZERO_KNOWLEDGE_INPUT_ON_CHAIN = 0x05;

    private readonly getShareWriter;
    private readonly engines: RealEngineState;
    private readonly keyGenerator: () => Elliptic.KeyPair;
    private readonly randomBytes: (l: number) => Buffer;

    constructor(engines: RealEngineState) {
        this.getShareWriter = RealBinaryShareWriter.create();
        this.engines = engines;
        this.keyGenerator = generateKeyPair;
        this.randomBytes = randomBytes;
    }

    private writeAndCreatePayload(
        rpc: BufferWriter,
        writeAdditional?: (stream: BufferWriter) => void
    ): Buffer {
        if (writeAdditional != null) {
            writeAdditional(rpc);
        }
        return rpc.toBuffer()
    }

    public onChainZkInput(
        shareWriterConsumer: (writer: RealBinaryShareWriter) => void,
        writeAdditional?: (stream: BufferWriter) => void
    ) {
        const rpc = new BufferWriter();
        const shares = this.writeShares(
            rpc,
            shareWriterConsumer,
            RealPayload.ZERO_KNOWLEDGE_INPUT_ON_CHAIN
        );
        const keyPair = this.keyGenerator();
        const publicKey = Buffer.from(keyPair.getPublic().encode("array", true));
        rpc.writeBuffer(publicKey);
        const buffers = [];
        for (let node = 0; node < this.engines.engines.length; node++) {
            const nodePublicKey = Buffer.from(this.engines.engines[node].publicKey, "base64");
            const aes = createAesForParty(keyPair, nodePublicKey);
            const encryptedShares = Buffer.concat([aes.update(shares[node]), aes.final()]);
            buffers.push(encryptedShares);
        }
        const shareBuffer = Buffer.concat(buffers);
        rpc.writeIntBE(shareBuffer.length);
        rpc.writeBuffer(shareBuffer);
        return this.writeAndCreatePayload(rpc, writeAdditional);
    }

    private writeShares(
        rpc: BufferWriter,
        shareWriterConsumer: (writer: RealBinaryShareWriter) => void,
        invocation: number
    ) {
        rpc.writeByte(invocation);
        const writer = RealBinaryShareWriter.create();
        shareWriterConsumer(writer);
        const bitLengths = writer.bitLengths();
        rpc.writeIntBE(bitLengths.length);
        for (const bitLength of bitLengths) {
            rpc.writeIntBE(bitLength);
        }
        return writer.toShares(this.randomBytes);
    }
}
