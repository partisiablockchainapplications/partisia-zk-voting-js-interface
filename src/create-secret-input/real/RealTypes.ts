import BN from "bn.js"

export interface RealEngineState {
    engines: RealEngineInformation[];
}

export interface RealEngineInformation {
    publicKey: string;
    restInterface: string;
}

export { BN };