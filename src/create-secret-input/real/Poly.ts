import { FiniteFieldElement } from "./FiniteFieldElement";

/**
 * A representation of a polynomial over a finite field.
 */
export interface Polynomial<T extends FiniteFieldElement<T>> {
  /**
   * The coefficients of the polynomial.
   */
  coefficients: T[];
  /**
   * The degree.
   */
  degree: number;
  /**
   * The constant term/
   */
  constantTerm: T;
  /**
   * Evaluate the polynomial at a given point.
   *
   * @param point the point
   * @returns the evaluated T
   */
  evaluate: (point: T) => T;
  /**
   * The zero element.
   */
  zero: T;
  /**
   * The one element.
   */
  one: T;
}

const filterHighZeroes = <T extends FiniteFieldElement<T>>(coefficients: T[], zero: T) => {
  for (let i = coefficients.length - 1; i >= 0; i--) {
    if (!coefficients[i].isZero()) {
      return coefficients.slice(0, i + 1);
    }
  }
  return [zero];
};

/**
 * Create a polynomial given the coefficients.
 *
 * @param coefficients the coefficients of the poly
 * @param zero the zero element
 * @param one the one element
 */
const createPolynomial = function <T extends FiniteFieldElement<T>>(
  coefficients: T[],
  zero: T,
  one: T
): Polynomial<T> {
  const filteredCoefficients = filterHighZeroes(coefficients, zero);
  return {
    coefficients: filteredCoefficients,
    constantTerm: coefficients[0],
    degree: filteredCoefficients.length - 1,
    zero,
    one,
    evaluate(point: T): T {
      const degree = this.degree;

      let result = this.coefficients[degree];
      for (let i = degree - 1; i >= 0; --i) {
        const current = this.coefficients[i];
        result = current.add(point.multiply(result));
      }
      return result;
    },
  };
};

export const Polynomial = {
  create: createPolynomial,
};
