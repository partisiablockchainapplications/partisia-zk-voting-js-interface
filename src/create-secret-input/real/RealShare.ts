import BN from "bn.js";

type Add<T> = (a: T, b: T) => T;
type Mul<T> = (a: T, b: T) => T;

/**
 * Compute f(x) where f is given by a set of coefficients.
 *
 * @param x the point to evaluate f in
 * @param coefficients description of f. Constant term in zeroth position
 * @param add a function to add two things
 * @param mul a function to multiply two things
 */
function evaluatePolynomial<T>(x: T, coefficients: T[], add: Add<T>, mul: Mul<T>): T {
  const degree = coefficients.length - 1;
  let result = coefficients[degree];
  for (let i = degree - 1; i >= 0; i--) {
    const c = coefficients[i];
    result = add(c, mul(x, result));
  }
  return result;
}

const computationAlphas = [1, 2, 3, 4];

export function initializeShareBuffers(n: number, size: number): Buffer[] {
  const shares: Buffer[] = [];
  for (let i = 0; i < n; i++) {
    shares.push(Buffer.alloc(size));
  }
  return shares;
}

export function writeShares(shareBytes: Buffer, shares: Buffer, offset: number) {
  const length = shareBytes.length;
  for (let i = 0; i < length; i++) {
    // offset points to end of the element being written, since we want to write it in big endian.
    shares.writeUInt8(shareBytes[length - i - 1], offset - i);
  }
}

export const ArithmeticRealHelper = {
  prime: new BN("7fffffffffffffffffffffffffffffff", 16),

  shareSize: 16,

  createShares: (values: BN[], random: (ln: number) => Buffer): Buffer[] => {
    const n = values.length;
    const shareSize = ArithmeticRealHelper.shareSize;
    const shares: Buffer[] = initializeShareBuffers(computationAlphas.length, shareSize * n);
    const mul = (a: BN, b: BN): BN => a.mul(b).mod(ArithmeticRealHelper.prime);
    const add = (a: BN, b: BN): BN => a.add(b).mod(ArithmeticRealHelper.prime);
    for (let i = 0; i < n; i++) {
      const secret = values[i];
      const coefficients = [secret, new BN(random(shareSize))];
      for (const alpha of computationAlphas) {
        const share = evaluatePolynomial(new BN(alpha), coefficients, add, mul);
        writeShares(share.toArrayLike(Buffer), shares[alpha - 1], shareSize * i + 15);
      }
    }
    return shares;
  },
};
