import { FiniteFieldElement } from "./FiniteFieldElement";
import { Polynomial } from "./Poly";

/**
 * Create a new field element with the given value.
 *
 * @param rawValue the value
 * @returns the created element
 */
const createElement = (rawValue: number): BinaryExtensionFieldElement => {
  return {
    value: rawValue & 0b1111,
    /** @inheritDoc */
    add(other: BinaryExtensionFieldElement): BinaryExtensionFieldElement {
      return Constants.elements[other.value ^ this.value];
    },

    /** @inheritDoc */
    subtract(other: BinaryExtensionFieldElement): BinaryExtensionFieldElement {
      return Constants.elements[other.value ^ this.value];
    },

    /** @inheritDoc */
    multiply(other: BinaryExtensionFieldElement): BinaryExtensionFieldElement {
      const index = other.value * 16 + this.value;
      return Constants.elements[Constants.lookup[index]];
    },

    /** @inheritDoc */
    modInverse(): BinaryExtensionFieldElement {
      return Constants.elements[Constants.multiplicativeInverse[this.value]];
    },

    /** @inheritDoc */
    negate(): BinaryExtensionFieldElement {
      return this;
    },

    /** @inheritDoc */
    squareRoot(): BinaryExtensionFieldElement {
      return Constants.elements[Constants.squareRootLookUp[this.value]];
    },

    /** @inheritDoc */
    isZero(): boolean {
      return this.value == 0;
    },

    /** @inheritDoc */
    isOne(): boolean {
      return this.value == 1;
    },

    /** @inheritDoc */
    isEqualTo(that: BinaryExtensionFieldElement): boolean {
      return that.value === this.value;
    },
  };
};

const ZERO: BinaryExtensionFieldElement = createElement(0);
const ONE: BinaryExtensionFieldElement = createElement(1);

/**
 * A factory that creates BinaryExtensionFieldElement.
 */
export const BinaryExtensionFieldElementFactory = {
  /**
   * Create a polynomial.
   * @param coefficients the coefficients of the polynomial
   */
  createPoly: (
    coefficients: BinaryExtensionFieldElement[]
  ): Polynomial<BinaryExtensionFieldElement> => {
    return Polynomial.create(coefficients, ZERO, ONE);
  },
  /**
   * Get the computation alphas for this field.
   * @returns the computation alphas
   */
  computationAlphas: () => {
    return Constants.computationAlphas;
  },
  /**
   * Create a new field element with the given value.
   * @param value the value
   */
  createElement,
  /**
   * The zero element.
   */
  zero: ZERO,
  /**
   * The one element.
   */
  one: ONE,
} as const;

const Constants = {
  elements: [
    createElement(0),
    createElement(1),
    createElement(2),
    createElement(3),
    createElement(4),
    createElement(5),
    createElement(6),
    createElement(7),
    createElement(8),
    createElement(9),
    createElement(10),
    createElement(11),
    createElement(12),
    createElement(13),
    createElement(14),
    createElement(15),
  ],
  computationAlphas: [createElement(1), createElement(2), createElement(3), createElement(4)],
  multiplicativeInverse: [
    0, 0x01, 0x09, 0x0e, 0x0d, 0x0b, 0x07, 0x06, 0x0f, 0x02, 0x0c, 0x05, 0x0a, 0x04, 0x03, 0x08,
  ],
  squareRootLookUp: [
    0, 0x01, 0x05, 0x04, 0x02, 0x03, 0x07, 0x06, 0x0a, 0x0b, 0x0f, 0x0e, 0x08, 0x09, 0x0d, 0x0c,
  ],
  lookup: [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0, 0x02, 0x04, 0x06, 0x08, 0x0a, 0x0c, 0x0e,
    0x03, 0x01, 0x07, 0x05, 0x0b, 0x09, 0x0f, 0x0d, 0, 0x03, 0x06, 0x05, 0x0c, 0x0f, 0x0a, 0x09,
    0x0b, 0x08, 0x0d, 0x0e, 0x07, 0x04, 0x01, 0x02, 0, 0x04, 0x08, 0x0c, 0x03, 0x07, 0x0b, 0x0f,
    0x06, 0x02, 0x0e, 0x0a, 0x05, 0x01, 0x0d, 0x09, 0, 0x05, 0x0a, 0x0f, 0x07, 0x02, 0x0d, 0x08,
    0x0e, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x03, 0x06, 0, 0x06, 0x0c, 0x0a, 0x0b, 0x0d, 0x07, 0x01,
    0x05, 0x03, 0x09, 0x0f, 0x0e, 0x08, 0x02, 0x04, 0, 0x07, 0x0e, 0x09, 0x0f, 0x08, 0x01, 0x06,
    0x0d, 0x0a, 0x03, 0x04, 0x02, 0x05, 0x0c, 0x0b, 0, 0x08, 0x03, 0x0b, 0x06, 0x0e, 0x05, 0x0d,
    0x0c, 0x04, 0x0f, 0x07, 0x0a, 0x02, 0x09, 0x01, 0, 0x09, 0x01, 0x08, 0x02, 0x0b, 0x03, 0x0a,
    0x04, 0x0d, 0x05, 0x0c, 0x06, 0x0f, 0x07, 0x0e, 0, 0x0a, 0x07, 0x0d, 0x0e, 0x04, 0x09, 0x03,
    0x0f, 0x05, 0x08, 0x02, 0x01, 0x0b, 0x06, 0x0c, 0, 0x0b, 0x05, 0x0e, 0x0a, 0x01, 0x0f, 0x04,
    0x07, 0x0c, 0x02, 0x09, 0x0d, 0x06, 0x08, 0x03, 0, 0x0c, 0x0b, 0x07, 0x05, 0x09, 0x0e, 0x02,
    0x0a, 0x06, 0x01, 0x0d, 0x0f, 0x03, 0x04, 0x08, 0, 0x0d, 0x09, 0x04, 0x01, 0x0c, 0x08, 0x05,
    0x02, 0x0f, 0x0b, 0x06, 0x03, 0x0e, 0x0a, 0x07, 0, 0x0e, 0x0f, 0x01, 0x0d, 0x03, 0x02, 0x0c,
    0x09, 0x07, 0x06, 0x08, 0x04, 0x0a, 0x0b, 0x05, 0, 0x0f, 0x0d, 0x02, 0x09, 0x06, 0x04, 0x0b,
    0x01, 0x0e, 0x0c, 0x03, 0x08, 0x07, 0x05, 0x0a,
  ],
} as const;

/**
 * A binary extension field element.
 */
export interface BinaryExtensionFieldElement
  extends FiniteFieldElement<BinaryExtensionFieldElement> {
  /**
   * The raw value of the element.
   */
  readonly value: number;
}
