import BN from "bn.js";

export class BufferWriter {
    private buffer: Buffer;

    constructor() {
        this.buffer = Buffer.alloc(0);
    }

    public readonly writeByte = (byte: number) => {
        const buffer = Buffer.alloc(1);
        buffer.writeUInt8(byte, 0);
        this.appendBuffer(buffer);
    };

    public readonly writeIntBE = (int: number) => {
        const buffer = Buffer.alloc(4);
        buffer.writeInt32BE(int, 0);
        this.appendBuffer(buffer);
    };

    public readonly writeIntLE = (int: number) => {
        const buffer = Buffer.alloc(4);
        buffer.writeInt32LE(int, 0);
        this.appendBuffer(buffer);
    };

    public readonly writeLongBE = (long: BN) => {
        this.writeNumberBE(long, 8);
    };

    public readonly writeLongLE = (long: BN) => {
        this.writeNumberLE(long, 8);
    };

    public readonly writeNumberBE = (num: BN, byteCount: number): void => {
        const buffer = num.toTwos(byteCount * 8).toArrayLike(Buffer, "be", byteCount);
        this.appendBuffer(buffer);
    };

    public readonly writeUnsignedNumberBE = (num: BN, byteCount: number): void => {
        const buffer = num.toArrayLike(Buffer, "be", byteCount);
        this.appendBuffer(buffer);
    };

    public readonly writeNumberLE = (num: BN, byteCount: number): void => {
        const buffer = num.toTwos(byteCount * 8).toArrayLike(Buffer, "le", byteCount);
        this.appendBuffer(buffer);
    };

    public readonly writeUnsignedNumberLE = (num: BN, byteCount: number): void => {
        const buffer = num.toArrayLike(Buffer, "le", byteCount);
        this.appendBuffer(buffer);
    };

    public readonly writeBuffer = (buffer: Buffer) => {
        this.appendBuffer(buffer);
    };

    public readonly writeDynamicBuffer = (buffer: Buffer) => {
        this.writeIntBE(buffer.length);
        this.writeBuffer(buffer);
    };

    public readonly writeBoolean = (bool: boolean) => {
        const buffer = Buffer.alloc(1);
        buffer.writeUInt8(bool ? 1 : 0, 0);
        this.appendBuffer(buffer);
    };

    public readonly writeStringBE = (str: string) => {
        const strBuffer = Buffer.from(str, "utf8");
        this.writeIntBE(strBuffer.length);
        this.appendBuffer(strBuffer);
    };

    public readonly writeStringLE = (str: string) => {
        const strBuffer = Buffer.from(str, "utf8");
        this.writeIntLE(strBuffer.length);
        this.appendBuffer(strBuffer);
    };

    public readonly writeHexString = (hex: string) => {
        this.appendBuffer(Buffer.from(hex, "hex"));
    };

    public readonly toBuffer = () => {
        return this.buffer.slice();
    };

    private readonly appendBuffer = (buffer: Buffer) => {
        this.buffer = Buffer.concat([this.buffer, buffer]);
    };
}
