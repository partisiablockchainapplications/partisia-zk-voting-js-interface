import BN from "bn.js";

export class BinaryShareWriter {
  private static readonly ONE = new BN(1);

  private index = 8;
  private readonly bitSet: number[] = [];

  /**
   * Create a BinaryShareWriter.
   *
   * @returns the created BinaryShareWriter
   */
  public static create() {
    return new BinaryShareWriter();
  }

  /**
   * Write a number of bytes from a byte buffer.
   *
   * @param buffer buffer containing the bytes to be written
   * @param offset starting offset of byte buffer
   * @param length number of bytes to write from offset
   */
  public writeBytes(buffer: Buffer, offset = 0, length = buffer.length): BinaryShareWriter {
    if (this.index === 8) {
      for (let i = offset; i < length; i++) {
        this.bitSet.push(buffer[i]);
      }
    } else {
      for (let i = offset; i < length; i++) {
        this.writeNumber(buffer[i], 8);
      }
    }
    return this;
  }

  /**
   * Write the specified number of bits of a BN to this builder.
   *
   * @param value the BN to write
   * @param bitLength the number of bits of the BN to write
   * @returns the BinaryShareWriter with the written BN
   */
  public writeLong(value: BN, bitLength: number): BinaryShareWriter {
    BinaryShareWriter.isValueValid(value, bitLength);

    const fullBytes = bitLength >> 3;
    const asBuffer = value.toArrayLike(Buffer, "le", fullBytes + 1);
    this.writeBytes(asBuffer, 0, fullBytes);
    const remainingBits = bitLength - fullBytes * 8;
    if (remainingBits > 0) {
      this.writeNumber(asBuffer[fullBytes], remainingBits);
    }
    return this;
  }

  /**
   * Write the specified number of bits of a number to this builder.
   *
   * @param value the number to write
   * @param bitLength the number of bits of the number to write
   * @returns the BinaryShareWriter with the written number
   */
  public writeInt(value: number, bitLength: number): BinaryShareWriter {
    if (bitLength > 32) {
      throw new Error(">32 bit numbers must be written using BinaryShareWriter#writeLong");
    }
    BinaryShareWriter.isValueValid(new BN(value), bitLength);
    return this.writeNumber(value, bitLength);
  }

  private writeNumber(value: number, bitLength: number) {
    for (let i = 0; i < bitLength; i++) {
      const isBitSet: boolean = (value & (1 << i)) !== 0;
      this.writeBoolean(isBitSet);
    }
    return this;
  }

  /**
   * Write the specified boolean as shares to this builder.
   *
   * @param bool the boolean to write
   * @returns the BinaryShareWriter with the written boolean
   */
  public writeBoolean(bool: boolean): BinaryShareWriter {
    if (this.index === 8) {
      this.index = 0;
      this.bitSet.push(0);
    }
    if (bool) {
      const bit = this.index;
      this.bitSet[this.bitSet.length - 1] = this.bitSet[this.bitSet.length - 1] | (1 << bit);
    }
    this.index++;
    return this;
  }

  public writtenBits(): number {
    return this.bitSet.length * 8 - (8 - this.index);
  }

  /**
   * Create a Buffer from the written values.
   *
   * @returns the created Buffer
   */
  public toBuffer(): Buffer {
    return Buffer.from(this.bitSet);
  }

  private static isValueValid(value: BN, bitLength: number) {
    const max = BinaryShareWriter.ONE.shln(bitLength).sub(BinaryShareWriter.ONE);
    if (value.gt(max)) {
      throw new Error(value + " cannot be represented as a " + bitLength + " bit number");
    }
    return max;
  }
}
