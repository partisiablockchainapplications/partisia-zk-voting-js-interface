import { createCipheriv } from "crypto";
import { BNInput, ec as Elliptic } from "elliptic";
import { sha256 } from "hash.js";
import BN from "bn.js";

const ec = new Elliptic("secp256k1");

export function generateKeyPair() {
    return ec.genKeyPair();
}

export function createSharedKey(keyPair: Elliptic.KeyPair, publicKey: Buffer) {
    const pairFromBuffer = ec.keyFromPublic(publicKey);
    const sharedRandom: BN = keyPair.derive(pairFromBuffer.getPublic());

    let sharedBuffer = sharedRandom.toArrayLike(Buffer, "be");
    if (sharedRandom.bitLength() % 8 === 0) {
        // Ensure that a sign bit is present in the byte encoding
        sharedBuffer = Buffer.concat([Buffer.alloc(1), sharedBuffer]);
    }
    return hashBuffer(sharedBuffer);
}

export function createAesForParty(keyPair: Elliptic.KeyPair, publicKey: Buffer) {
    const sharedKey = createSharedKey(keyPair, publicKey);
    const iv = sharedKey.slice(0, 16);
    const secretKey = sharedKey.slice(16, 32);
    return createCipheriv("aes-128-cbc", secretKey, iv);
}

export function signTransaction(data: BNInput, privateKey: string): Buffer {
    const keyPair = privateKeyToKeypair(privateKey);
    const signature = keyPair.sign(data);
    return signatureToBuffer(signature);
}

export function signatureToBuffer(signature: Elliptic.Signature): Buffer {
    if (signature.recoveryParam == null) {
        throw new Error("Recovery parameter is null");
    }
    return Buffer.concat([
        Buffer.from([signature.recoveryParam]),
        signature.r.toArrayLike(Buffer, "be", 32),
        signature.s.toArrayLike(Buffer, "be", 32),
    ]);
}

export function keyPairToAccountAddress(keyPair: Elliptic.KeyPair): string {
    const publicKey = keyPair.getPublic(false, "array");
    const hash = sha256();
    hash.update(publicKey);
    return "00" + hash.digest("hex").substring(24);
}

export function privateKeyToKeypair(privateKey: string) {
    return ec.keyFromPrivate(privateKey, "hex");
}

export function privateKeyToPublicKey(privateKey: string) {
    const keyPair = privateKeyToKeypair(privateKey);
    return Buffer.from(keyPair.getPublic(true, "array"));
}

export function privateKeyToAccountAddress(privateKey: string) {
    return keyPairToAccountAddress(privateKeyToKeypair(privateKey));
}

export function publicKeyToAccountAddress(publicKey: Buffer) {
    return keyPairToAccountAddress(ec.keyFromPublic(publicKey));
}

export function hashBuffers(buffers: Buffer[]): Buffer {
    const hash = sha256();

    for (const buffer of buffers) {
        hash.update(buffer);
    }

    return Buffer.from(hash.digest());
}

export const hashBuffer = (buffer: Buffer): Buffer => {
    return hashBuffers([buffer]);
};

export const createShares = (buffer: Buffer, randomBytes: (l: number) => Buffer) => {
    const blindedShares = Buffer.concat([randomBytes(32), buffer]);
    const first = randomBytes(blindedShares.length);
    const second = randomBytes(blindedShares.length);
    const third = xor(xor(first, second), blindedShares);
    return [first, second, third];
};

export const commitShares = (shares: Buffer[]) => {
    if (shares.length === 3) {
        return [hashBuffer(shares[0]), hashBuffer(shares[1]), hashBuffer(shares[2])];
    } else {
        throw new Error("Unsupported number of shares");
    }
};

export const xor = (left: Buffer, right: Buffer) => {
    const result = Buffer.from(left);
    xorInPlace(result, right);
    return result;
};

export const xorInPlace = (target: Buffer, other: Buffer) => {
    for (let i = 0; i < target.length; i++) {
        target[i] ^= other[i];
    }
};
