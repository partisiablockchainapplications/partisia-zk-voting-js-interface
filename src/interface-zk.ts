export interface IContractZk {
  type: string
  address: string
  jarHash: string
  storageLength: number
  serializedContract: SerializedContract
  abi: string
}

export interface SerializedContract {
  attestations: any[]
  calculationStatus: string
  computationTriples: any[]
  engines: Engines
  finishedComputations: any[]
  inputMaskBatchesOrderedInTotal: number
  inputMasks: number[]
  nextAttestationId: number
  nextInputMaskOffset: number
  nextVariableId: number
  nodeRegistryContract: string
  openState: OpenState
  pendingInput: any[]
  pendingOnChainOpenUser: PendingOnChainOpenUser
  preprocessContract: string
  variables: Variable[]
  zkComputationDeadline: string
}

export interface Engines {
  engines: Engine[]
}

export interface Engine {
  identity: string
  publicKey: string
  restInterface: string
}

export interface OpenState {
  openComputationVariables: any[]
  openState: InformationClass
}

export interface InformationClass {
  data: string
}

export interface PendingOnChainOpenUser {
  nextId: number
  pending: any[]
}

export interface Variable {
  key: number
  value: Value
}

export interface Value {
  id: number
  information: InformationClass
  inputMaskOffset: number
  maskedInputShare: InformationClass
  owner: string
  sealed: boolean
  shareBitLengths: number[]
  transaction: string
}
