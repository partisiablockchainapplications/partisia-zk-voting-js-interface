import { AbiParser, JsonValueConverter, StateReader } from 'abi-client-ts'
import assert from 'assert'
import { partisiaCrypto } from 'partisia-crypto'
import { PartisiaAccount, PartisiaRpc } from 'partisia-rpc'
import { IPartisiaRpcConfig, PartisiaAccountClass } from 'partisia-rpc/lib/main/accountInfo'
import { IContractZk } from 'partisia-rpc/lib/main/interface-zk'
import { PartisiaRpcClass } from 'partisia-rpc/lib/main/rpc'
import { buildSecretInputRpc } from './create-secret-input/SecretInputCreator'
import { IVotingState } from './interface'

export class VotingContractZk {
  abi?: string
  contractAddress: string
  rpc: PartisiaAccountClass

  constructor(contractAddress: string, rpc: IPartisiaRpcConfig, abi?: string) {
    this.abi = abi
    this.contractAddress = contractAddress
    this.rpc = PartisiaAccount(rpc)
  }
  async initAbi() {
    const contract = await this.rpc.getContract(this.contractAddress, this.rpc.deriveShardId(this.contractAddress), true)
    this.abi = contract?.data?.abi
  }
  async getEngines() {
    // get engines
    const contract = await this.rpc.getContract(this.contractAddress, this.rpc.deriveShardId(this.contractAddress), true)
    const data = contract.data as IContractZk
    const engines = data.serializedContract!.engines

    return engines
  }
  async getContractWhitelist(): Promise<string[]> {
    return (await this.getContractState())?.whitelist || []
  }
  async getContractResult(): Promise<number[]> {
    const state: IVotingState = await this.getContractState()
    if (state.voting_result.isSome) {
      return state.voting_result.innerValue.votes
    } else {
      return []
    }
  }
  async getContractState(): Promise<IVotingState> {
    const contract = await this.rpc.getContract(this.contractAddress, null, true)
    if (contract == null) return null
    if (!this.abi) await this.initAbi()

    // deserialize state
    const fileAbi = new AbiParser(Buffer.from(this.abi, 'base64')).parseAbi()
    const serializedContract = contract.data.serializedContract! as any
    const reader = new StateReader(Buffer.from(serializedContract.openState.openState.data, 'base64'), fileAbi.contract)
    const struct = reader.readStruct(fileAbi.contract.getStateStruct())
    return JsonValueConverter.toJson(struct) as any
  }

  async seeFinalResult() {
    // TODO
  }

  async castVote(
    privateKey: string | Buffer,
    vote: 0 | 1 | 2 | 3 | number,
    isMainnet: boolean = false,
    cost: number | string = 35000
  ): Promise<{
    isFinalOnChain: boolean
    trxHash: string
    hasError: boolean
    errorMessage?: string
  }> {
    const wif = typeof privateKey === 'string' ? privateKey : privateKey.toString('hex')
    const address = partisiaCrypto.wallet.privateKeyToAccountAddress(wif)

    const shardId = this.rpc.deriveShardId(address)
    const url = this.rpc.getShardUrl(shardId)
    const nonce = await this.rpc.getNonce(address, shardId)

    const engines = await this.getEngines()
    const bidShortName = 0x01
    const payload = buildSecretInputRpc(engines, bidShortName, vote)

    const serializedTransaction = partisiaCrypto.transaction.serializedTransaction(
      {
        nonce,
        cost,
      },
      {
        contract: this.contractAddress,
      },
      payload
    )
    const digest = partisiaCrypto.transaction.deriveDigest(`Partisia Blockchain${isMainnet ? '' : ' Testnet'}`, serializedTransaction)
    const signature = partisiaCrypto.wallet.signTransaction(digest, wif)
    const trx = partisiaCrypto.transaction.getTransactionPayloadData(serializedTransaction, signature)

    const trxHash = partisiaCrypto.transaction.getTrxHash(digest, signature)
    const rpcShard = PartisiaRpc({ baseURL: url })
    const isValid = await rpcShard.broadcastTransaction(trx)
    assert(isValid, 'Unknown Error')

    const isFinalOnChain = await broadcastTransactionPoller(trxHash, rpcShard)

    // check for errors
    let res
    if (isFinalOnChain) {
      res = await this.rpc.getTransactionEventTrace(trxHash)
    } else {
      res = {
        hasError: true,
        errorMessage: 'unable to broadcast to chain',
      }
    }

    return {
      isFinalOnChain,
      trxHash,
      ...res,
    }
  }
}

export const broadcastTransactionPoller = async (trxHash: string, rpc: PartisiaRpcClass, num_iter = 10, interval_sleep = 2000) => {
  let intCounter = 0
  while (++intCounter < num_iter) {
    try {
      console.log('intCounter', intCounter)
      const resTx = await rpc.getTransaction(trxHash)
      if (resTx.finalized) {
        break
      }
    } catch (error) {
      console.error(error.message)
    } finally {
      const sleep = (ms) => {
        return new Promise((resolve) => setTimeout(resolve, ms))
      }
      await sleep(interval_sleep)
    }
  }
  return intCounter < num_iter
}
