export interface IVotingState {
  owner: string
  whitelist: string[]
  voting_info: VotingInfo
  current_voter_id: number
  voters: any[]
  voting_result: VotingResult
}

export interface VotingInfo {
  name: string
  description: string
  voting_options: VotingOption[]
}

export interface VotingOption {
  title: string
}

export interface VotingResult {
  isSome: boolean
  innerValue?: { votes: number[] }
}
