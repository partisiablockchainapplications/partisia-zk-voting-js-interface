### Voting Contract JavaScript Interface

Source WASM and ABI for contract: `https://github.com/INSNetwork/zk-vote/tree/main/whitelisted_voting` `feature/6-vote-options`
Paris testnet deploy: `https://testnet.mpcexplorer.com/address/03c0ae04c4ca74c3bafea9ca179f553aebb33f0e60`

### config testnet

```ts
const rpcConfig = {
  urlBaseGlobal: { url: 'https://node1.testnet.partisiablockchain.com', shard_id: 99 },
  urlBaseShards: [
    { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard0', shard_id: 0 },
    { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard1', shard_id: 1 },
    { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard2', shard_id: 2 },
  ],
}
const addressContract = '03c0ae04c4ca74c3bafea9ca179f553aebb33f0e60'
```

### Example Cast Vote directly on Chain with Private Key

```ts
;(async () => {
  const PRIVATE_KEY = '<64 len hex string>'
  const vote_option = 0 // 0, 1, 2, 3

  const votingContract = new VotingContract(addressContract, rpcConfig)
  const res = await votingContract.castVote(PRIVATE_KEY, vote_option, false, 35000)
  console.log('res', JSON.stringify(res, null, 2))

  // Expected output:
  /* 
 {
  "isFinalOnChain": true,
  "trxHash": "054832b76b571db97c5894e9bd2fbc12cdc3587e47e478ca6e99aa08d9357e09",
  "hasError": false,
  "eventTrace": [
    {
      "shardId": 0,
      "txHash": "81d21b600de076f6a592dea024f3a203e56dec2fa9d33888f1ee3135cce33cdb"
    },
    {
      "shardId": 2,
      "txHash": "edaa4e5a8cfbe7bef711e0b82d76d6e1e73b19efa13a7c5ca68a0e101f3b8910"
    }
  ]
} */
})()
```

### Example Get Contract State

```ts
;(async () => {
  const votingContract = new VotingContract(addressContract, rpcConfig)
  const res = await votingContract.getContractState()
  console.log('res', JSON.stringify(res, null, 2))

  // Expected output:
  /* 
{
  "owner": "00b48ffc5d275744b14b609f80180c44ef77992aac",
  "whitelist": [
    "00b48ffc5d275744b14b609f80180c44ef77992aac",
    "00a7e41597691d4a46b1871a6d82b32c9a8329b563",
    "008e9ef52d67add43f6f2d00865a1128da83bd183b"
  ],
  "voting_info": {
    "name": "ZK Vote",
    "description": "Zero Knowledge",
    "voting_options": [
      {
        "title": "Option 1"
      },
      {
        "title": "Option 2"
      },
      {
        "title": "Option 3"
      },
      {
        "title": "Option 4"
      }
    ]
  },
  "current_voter_id": 3,
  "voters": [
    {
      "key": "008e9ef52d67add43f6f2d00865a1128da83bd183b",
      "value": 2
    },
    {
      "key": "00a7e41597691d4a46b1871a6d82b32c9a8329b563",
      "value": 3
    },
    {
      "key": "00b48ffc5d275744b14b609f80180c44ef77992aac",
      "value": 1
    }
  ],
  "voting_result": {
    "isSome": true,
    "innerValue": {
      "votes": [
        2,
        1,
        0,
        0
      ]
    }
  }
}
 */
})()
```
### Example Get Whitelist addresses

```ts
;(async () => {
  const votingContract = new VotingContract(addressContract, rpcConfig)
  const res = await votingContract.getContractWhitelist()
  console.log('res', JSON.stringify(res, null, 2))

  // Expected output:
  /* 
[
  "00b48ffc5d275744b14b609f80180c44ef77992aac",
  "00a7e41597691d4a46b1871a6d82b32c9a8329b563",
  "008e9ef52d67add43f6f2d00865a1128da83bd183b"
]
 */
})()
```

### Example Get Voting Results at end of vote

```ts
;(async () => {
  const votingContract = new VotingContract(addressContract, rpcConfig)
  const res = await votingContract.getContractResult()
  console.log('res', JSON.stringify(res, null, 2))

  // Expected output:
  /* 
 [
  2,
  1,
  0,
  0
] 
*/
})()
```
