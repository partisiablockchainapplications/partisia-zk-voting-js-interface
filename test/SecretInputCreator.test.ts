import { buildSecretInputRpc } from "../src/create-secret-input/SecretInputCreator"
import { RealEngineInformation, RealEngineState } from "../src/create-secret-input/real/RealTypes"

function testEngine(publicKey: string, restInterface: string): RealEngineInformation {
  return {
    publicKey: publicKey,
    restInterface: '',
  }
}

export function testNetEngineState(): RealEngineState {
  return {
    engines: [
      testEngine('Ax3kZlMV9JW6EE/74YO9X8Y7zVeD8TubNlBaY+IMfARg', 'https://node1.testnet.partisiablockchain.com'),
      testEngine('Awndg7zaEkVJxHgNzaITUUX+uzAXYl9Ja9X8QPcRy9pZ', 'https://node2.testnet.partisiablockchain.com'),
      testEngine('A9SqNrfygSuXOLNsdy4Gx8d0kSV5S/ET7GCnTVz90FQ7', 'https://node3.testnet.partisiablockchain.com'),
      testEngine('Ari1IPP44JCkTfB98nVyMn0y2bZwgkQEEvrBV/umVkUo', 'https://node4.testnet.partisiablockchain.com'),
    ],
  }
}

test('send auction bid', () => {
  const bidShortName = 0x40
  const bidValue = 5
  const payload = buildSecretInputRpc(testNetEngineState(), bidShortName, bidValue)
  console.log(payload.toString('hex'))
})
